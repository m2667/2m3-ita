# Note sulle presentazione su Italo Calvino

## Il visconte dimezzato

|> Il visconte Medardo va alla guerra contre i turchi

|> E dimezzato con un cannone

## Il castello dei destini incrociati

|> Il narratore viaggio e visita un castello per dormire

|> Mangia con gli altri ma non puo parlare

|> Scopra che possono communicare con delle carte di tarocchi

|> Campo lessicale del silenzio

|> Casualita: accidento

|> Storia medievale
