# Test Gramatica Corrige

## Esercizio 1

    > mia
    > suo
    > le sue
    > il suo
    > i suoi
    > le loro
    > la sua
    > suo

## Esercizio 2

    > vive
    > avranno
    > partira`
    > capisco
    > fumo
    > 
    >
    >
    > andro`; viaggero`
    > paghero

## Esercizio 3

    > e` stato
    > abbiamo trovato
    > riguardavano
    > si e` diplomato
    > era
    > ha provato
    > ha chiesto
    > ha ricevuto
    > ha deciso
    > si e` iscritto
    > c'era/c'e` stato
    > dovevano
    > erano/sono state
    > hanno fornito
    > poterano
    > si sono iscritte
    > e` tornata

## Esercizio 4

    > la
    > mi/ti/gli/ci
    > lo
    > li
    > chi (?)
    > lo
    > li/l'
    > la/l'
