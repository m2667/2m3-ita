# Note sulle presentazione sulle citta italiane

## Bari

|> Orechietto alle cime

|> Il migliore ristoranto e in una grotta

|> Visitare Bari Vecchia

|> Trulli di Alberobello (Trulli => capanne di pietra, since neolithic)

|> Onofrio Martinelli, pittore

## Genova

|> Liguria, capoluogo

|> Catedrale di San Lorenzo, stile gotico, completamenta 1490

|> Pasta al pesto

|> Aquario di genova

|> Boccadasse: villagio tipico, vicino

|> Giuseppo Mazzini: patriota

## Verona

|> Basilica di san Zeno

|> Il colosseo

|> "Citta di amore"

|> Luogo di "Romeo e Giulietta" di Sheakspeare

|> Gnocchi

|> Visitare la casa di Giulietta

|> Guardare un spectacolo al colosseo

## Ancona

|> Capoluogo della provincia delle Marche

|> Passato greco-romano

|> Cattedrale San Ciriaco

|> Chiesa romana-bizantina

|> Vincisgrassi e llasagna

|> Mole Vanviteliana

|> Vito Volterra -> Matematico e fisico italiano

## Bergamo

|> Regione di lombardia

|> Mura venete di Bergamo -> parte dell'UNESCO

|> Gialli a forma di cupola, polenta dolce e uccelini di cioccolate

|> Piazza vecchia -> Cuore di belgamo alta

|> Palazzo della ragione

|> Museo degli Affreschi

## Lecce

|> Specializatta nella cartapesta

|> Cibi: la puccia (panino con prosciutto crudo, olio, formaggio e carciofi)

|> Visitare la basilica Santa Croce o il anfitetro

|> Tancredi di sicilia => primo conte di Lecce, re di sicilia

|> "Firenza del sud" => citta bella (stile baroc)

|> Vicina al adriatico

## Palermo

|> Capoluogo della Sicilia

|> Piu antica residenze reale dell'Europa

|> Cibi: Canelli piatti di besce e arancine

|> Attivita: monumenti storici, mare, cibi

|> Giovanni Falcone: Giudice, lotta contro la mafia, e ucciso della mafia

## Torino

|> Capoluogo del piemonte

|> Quarta communa italiana per populazione

|> Il duomo e la piazza castello (piazza centrale)

|> Risotto al barolo

|> Visitare il Palazzo Reale

|> Fabrica del Fiat

|> Giovanni Anelli: Proprietario e dirrettore del Fiat, nato e morto a Torino

## Siracusa

|> In Sicilia

|> Teatro Geco, tempio di Atena e castello di Maniace

|> Pasta fritta alla Siracusana

|> Visita guidata della vecchia Siracusa

|> Grotte intorno l'isola

|> Archimede e nato a Siracusa

## Venezia

|> Basilica de San Marco => Cuore della attivita turistica costruita nel 5 seccolo dc

|> Bigoli in salsa => piasta al sugo di anchois (?)

|> Visitare il ponte di Rialto

|> Antonio Vivaldi => violonista famoso
