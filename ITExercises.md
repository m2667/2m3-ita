# Essercizi d'Italiano

## Capitolo 1

### Esercizio 1-1

**Che mestiere fa Ferretti?**

|> Ferretti e un giornalisto.

>-> giornalist**a**

**Che ora è ?**

|> E le 10.

>->

**Qual è la reazione di Ferretti al nuovo incarico?**

|> Lui non vuole si occupare della cronica nera e dei deliti.

**Di che cosa si occupa normalmente Ferretti al giornale?**

|> Scriva sul Palio.

>-> Scriva **degli articoli di costume**

**Per quale tipo di giornale lavora Ferretti?**
|> Lavora per un giornale di articoli di costume.

>-> Lavora per un quotidiane

### Esercizio 1-4

1. Sono le due meno un quarto.
2. E l'una e mezzo.
3.
4.
5. Sono le mezzogiorno e dieci.

## Capitolo 2

### Esercizio 2-1

1. b
2. a
3. b
4. b
5. b
6. c

## Capitolo 3

### Esercizio 3-1

1. a
2. c
3. c
4. b
5. a
6. ?
7. a

### Esercizio 3-2

1. a
2. c
3. b

## Capitolo 4

### Esercizio 4-1

1. b
2. a
3. c
4. b
5. a
6. ?

> 6c

### Esercizio 4-2

Primi
> Pasta e fagioli

Secondi
> Ossibuchi

Contorni
> Patate al forno

Dolci
> Cantucci con il vino santo

### Esercizio 4-3

1. c
2. c
3. a
4. c
5. c
6. ?

> 6b
